Simple application using express.js with TypeScript on server side, and react as client.

1. You need ts-node to setup server
2. Use webpack on root dir to bundle front end scripts
3. Setup .env
4. You're ready to go