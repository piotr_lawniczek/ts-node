import fs from 'fs';
import path from 'path';

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const lessToJs = require('less-vars-to-js');
const themeVariables = lessToJs(fs.readFileSync(path.resolve(__dirname, 'src/client/less/ant-design-vars.less'), 'utf8'));


const config = {
    entry: {
        client: [
            'babel-polyfill',
            './src/client/app.js'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'index.js'
    },

    module: {
        rules: [{
                test: /\.(js|jsx)?$/,
                loader: 'babel-loader',
                include: [path.resolve(__dirname, 'src/client')],
                query: {
                    presets: [
                        'env',
                        'stage-2',
                        'react'
                    ],
                    plugins: [
                        ['import', {
                            libraryName: "antd",
                            style: true
                        }]
                    ]
                }
            },
            {
                test: /\.css$/,
                use: [
                    // {
                    //     loader: MiniCssExtractPlugin.loader,
                    //     options: {
                    //         publicPath: './public'
                    //     }
                    // },
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "less-loader",
                        options: {
                            javascriptEnabled: true,
                            modifyVars: themeVariables
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                include: [path.resolve(__dirname, 'src/client/scss')],
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    },
                    {
                        loader: "sass-loader" // compiles Sass to CSS
                    }
                ]
            },
        ]
    },
    // plugins: [
    //     new MiniCssExtractPlugin({
    //         // Options similar to the same options in webpackOptions.output
    //         // both options are optional
    //         filename: "public/[name].css",
    //         chunkFilename: "[id].css"
    //     })
    // ],
    // optimization: {
    //     splitChunks: {
    //         cacheGroups: {
    //             styles: {
    //                 name: 'styles',
    //                 test: /\.css$/,
    //                 chunks: 'all',
    //                 enforce: true
    //             }
    //         }
    //     }
    // },
    // devtool: 'cheap-module-source-map'
    // devtool: 'cheap-module-eval-source-map'
}

export default config;