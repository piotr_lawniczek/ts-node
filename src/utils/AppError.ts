export class AppError extends Error {

    public name: string;
    public message: string;
    public errno: number;
    public code: string;
    public httpStatus: number;

    public constructor(
        message: string,
        errno: number,
        httpStatus: number = 500,
        code?: string,
    ) {
        super(message);
        Object.setPrototypeOf(this, AppError.prototype);
        this.name = this.constructor.name;
        this.code = code || 'APPLICATION_ERROR';
        this.errno = errno;
        this.httpStatus = httpStatus;
    }

    static readonly NOT_FOUND = 1;
}