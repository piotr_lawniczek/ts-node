import { Entity } from 'typeorm';

export class Paginator {
    public constructor(
        public page: number,
        public records: number,
        public total: number,
        public items: Array<any> = []
    ) {

    }
}