export const TYPE = {
    PinRepository: Symbol('PinRepository'),
    FileRepository: Symbol('FileRepository'),
    TagRepository: Symbol('TagRepository'),
    CategoryRepository: Symbol('CategoryRepository'),

    CategoryService: Symbol('CategoryService'),
    FileService: Symbol('FileService'),
    PinService: Symbol('PinService'),
    TagService: Symbol('TagService')
};