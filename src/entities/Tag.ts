import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToMany,
    JoinTable
} from "typeorm";
import { Pin } from './Pin';


@Entity()
export class Tag {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({
        unique: true
    })
    public name!: string;

    @ManyToMany(type => Pin)
    @JoinTable()
    tags: Pin[];
}