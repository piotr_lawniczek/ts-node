import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
} from "typeorm";
import { Pin } from './Pin';


@Entity()
export class File {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public name!: string;

    @Column()
    public path!: string;

    @Column()
    public mimetype!: string;

    @Column()
    public extension!: string;

    @Column()
    public size!: number;

    @ManyToOne(type => Pin, pin => pin.files)
    public pin!: Pin;
}