import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    ManyToOne,
    ManyToMany,
    JoinTable
} from "typeorm";
import { MinLength } from "class-validator";
import { Tag } from './Tag';
import { File } from './File';
import { Category } from './Category';

@Entity()
export class Pin {

    @PrimaryGeneratedColumn()
    public id!: number;

    @MinLength(2)
    @Column()
    public name!: string;

    @Column({
        nullable: true,
        type: 'text'
    })
    public content: string;

    @ManyToOne(type => Category, category => category.pins)
    public category!: Category;

    @OneToMany(type => File, file => file.pin, {
        eager: true
    })
    public files: File[];

    @ManyToMany(type => Tag, {
        eager: true
    })
    @JoinTable()
    tags: Tag[];
} 