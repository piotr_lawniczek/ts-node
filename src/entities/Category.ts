import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany
} from "typeorm";
import { Pin } from './Pin'; 

@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({
        unique: true
    })
    public name!: string;

    @Column({
        nullable: true,
        type: 'text'
    })
    public description: string;

    @OneToMany(type => Pin, pin => pin.category)
    public pins: Pin[];
}