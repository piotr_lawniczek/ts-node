import * as express from "express";
import { inject } from "inversify";
import {
    BaseHttpController,
    controller,
    httpGet,
    httpPost,
    httpDelete,
    requestBody,
    requestParam,
    queryParam,
    response,
    httpPut
} from "inversify-express-utils";
import { Repository } from "typeorm";
import { TYPE } from "../constants/types";
import { Category } from "../entities/Category";
import { CategoryRequest } from '../models/category/CategoryRequest';
import { validate } from '../middlewares/Validator';
import { CategoryService } from '../services/CategoryService';
import { PinService } from '../services/PinService';
@controller(
    "/api/categories"
)
class CategoryController extends BaseHttpController {

    private readonly categoryService: CategoryService;
    private readonly pinService: PinService;

    public constructor(
        @inject(TYPE.CategoryService) categoryService: CategoryService,
        @inject(TYPE.PinService) pinService: PinService
    ) {
        super();
        this.categoryService = categoryService;
        this.pinService = pinService;
    }

    @httpGet("/")
    public async list(
        @response() res: express.Response,
        @queryParam('page') page: number,
        @queryParam('records') records: number
    ) {
        return await this.categoryService.getPaginator(+page, +records);
    }

    @httpGet("/:id")
    public async get(
        @response() res: express.Response,
        @requestParam('id') id: number
    ) {
        let category = await this.categoryService.find(id);
        return category;
    }

    @httpDelete("/:id")
    public async delete(
        @response() res: express.Response,
        @requestParam('id') id: number
    ) {
        return await this.categoryService.delete(id);
    }

    @httpPost("/", validate(CategoryRequest))
    public async post(
        @response() res: express.Response,
        @requestBody() categoryRequest: CategoryRequest
    ) {
        return await this.categoryService.create(categoryRequest);
    }

    @httpPut("/:id", validate(CategoryRequest))
    public async put(
        @response() res: express.Response,
        @requestParam('id') id: number,
        @requestBody() categoryRequest: CategoryRequest
    ) {
        return await this.categoryService.update(id, categoryRequest);
    }

    @httpGet("/:id/pins")
    public async pinsList(
        @response() res: express.Response,
        @requestParam('id') id: number,
        @queryParam('page') page: number,
        @queryParam('records') records: number,
        @queryParam('tags') tags: string[]
    ) {
        return await this.pinService.getPaginator(+page, +records, tags, id);
    }
}
