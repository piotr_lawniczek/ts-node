import * as express from "express";
import { inject } from "inversify";
import {
    BaseHttpController,
    controller,
    httpGet,
    httpPost,
    httpDelete,
    request,
    requestBody,
    requestParam,
    queryParam,
    response,
    httpPut
} from "inversify-express-utils";
import { Repository } from "typeorm";
import { TYPE } from "../constants/types";
import { Pin } from "../entities/Pin";
import { PinService } from "../services/PinService";
import { CreatePinRequest } from "../models/pin/CreatePinRequest";
import { validate } from "../middlewares/Validator";
import { PinView } from "../models/pin/PinView";
import { uploadFiles, multipartForm, uploadFile } from "../middlewares/MultipartForm";

@controller(
    "/api/pins"
)
export class PinController extends BaseHttpController {
    private readonly pinService: PinService;

    public constructor(@inject(TYPE.PinService) pinService: PinService) {
        super();
        this.pinService = pinService;
    }

    @httpGet("/")
    public async list(
        @response() res: express.Response,
        @queryParam('page') page: number,
        @queryParam('records') records: number,
        @queryParam('tags') tags: string[]
    ) {
        return await this.pinService.getPaginator(+page, +records, tags);
    }

    @httpGet("/:id")
    public async get(
        @response() res: express.Response,
        @requestParam('id') id: number
    ) {
        let pin = await this.pinService.find(id);
        return pin ? new  PinView(pin) : null;
    }

    @httpDelete("/:id")
    public async delete(
        @response() res: express.Response,
        @requestParam('id') id: number
    ) {
        return await this.pinService.delete(id);
    }


    @httpPost("/", multipartForm(), validate(CreatePinRequest))
    public async post(
        @response() res: express.Response,
        @request() request: express.Request
    ) {
        let pinRequest: CreatePinRequest = request.body;
        pinRequest.uploadedFiles = request.files; 
        let pin = await this.pinService.create(pinRequest);
        return pin ? new  PinView(pin) : null;
    }

    @httpPut("/:id", validate(CreatePinRequest))
    public async put(
        @response() res: express.Response,
        @requestParam('id') id: number,
        @requestBody() pinRequest: CreatePinRequest
    ) {
        let pin = await this.pinService.update(id, pinRequest);
        return pin ? new  PinView(pin) : null;
    }

    @httpPost("/:id/files", uploadFile('file'))
    public async filesPost(
        @response() res: express.Response,
        @requestParam('id') id: number,
        @request() request: express.Request
    ) {
        let uploadedFile = request.file; 
        let pin = await this.pinService.addFile(id, uploadedFile);
        return pin ? new  PinView(pin) : null;
    }

    @httpDelete("/:id/files/:fileId")
    public async filesDelete(
        @response() res: express.Response,
        @requestParam('id') id: number,
        @requestParam('fileId') fileId: number,
    ) {
        let pin = await this.pinService.deleteFile(id, fileId);
        return pin ? new  PinView(pin) : null;
    }
}