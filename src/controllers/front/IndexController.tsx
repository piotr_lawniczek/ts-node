import * as express from "express";
import { inject } from "inversify";
import {
    BaseHttpController,
    controller,
    httpGet,
    httpPost,
    httpDelete,
    request,
    requestBody,
    requestParam,
    queryParam,
    response,
    httpPut
} from "inversify-express-utils";

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from './../../client/reducers'
import * as React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import Html from './../../client/containers/Html.jsx';
import App from './../../client/containers/App.jsx';
import { TYPE } from "../../constants/types";
import { PinService } from "../../services/PinService";
import { CategoryService } from "../../services/CategoryService";

@controller('')
export class IndexController extends BaseHttpController {
    private readonly pinService: PinService;
    private readonly categoryService: CategoryService;

    public constructor(
        @inject(TYPE.PinService) pinService: PinService,
        @inject(TYPE.CategoryService) categoryService: CategoryService
    ) {
        super();
        this.pinService = pinService;
        this.categoryService = categoryService;
    }

    @httpGet('/')
    public async render(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const scripts = [
            // 'vendor.js',
            'index.js'
        ];
        let categories = await this.categoryService.getPaginator(1, 200);
        let pins = await this.pinService.getPaginator(1, 200);
        const initialState = {
            categories: categories.items,
            pins: pins.items
        };
        const store = createStore(reducers, initialState);
        const appMarkup = renderToString(
            <Provider store={store}>
                <App />
            </Provider>   
        );
        const html = renderToStaticMarkup(React.createElement(Html, {
            children: appMarkup,
            initialState: initialState,
            scripts: scripts
        }))

        res.send(`<!doctype html>${html}`);
    }
}