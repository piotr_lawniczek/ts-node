import { IsOptional, IsString, MinLength } from 'class-validator';
import { ICategoryData } from './ICategoryData';

export class CategoryRequest implements ICategoryData{

    @IsString()
    @MinLength(2)
    public name!: string;

    @IsOptional()
    @MinLength(5)
    public description: string;
}