import { Pin } from '../../entities/Pin';
import { File } from '../../entities/File';
import { Category } from '../../entities/Category';

export class PinView {
    public id!: number;
    public name!: string;
    public content: string;
    public tags?: string[];
    public files?: File[];
    public category?: Category;

    public constructor(pin: Pin) {
        this.id = pin.id;
        this.name = pin.name;
        this.content = pin.content;
        this.tags = pin.tags.map(tag => tag.name);
        this.files = pin.files;
        this.category = pin.category;
    }
}