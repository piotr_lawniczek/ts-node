import { Category } from '../../entities/Category';
import { Express } from 'express';
import { File } from '../../entities/File';
export interface IPinData {
    id?: number;
    name: string;
    content: string;
    tags?: string[];
    category: Category;
    uploadedFiles?: Express.Multer.File[] | { [fieldname: string]: Express.Multer.File[] };
    files?: File[];
}

