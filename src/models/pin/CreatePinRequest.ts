import { IsOptional, IsString, MinLength, ValidateNested, IsDefined } from 'class-validator';
import { IPinData } from './IPinData';
import { Category } from '../../entities/Category';
import { Stream } from 'stream';
import { Express } from 'express';

export class CreatePinRequest implements IPinData {

    @IsString()
    @MinLength(2)
    public name!: string;

    @IsOptional()
    @MinLength(2)
    public content: string;

    @IsOptional()
    @MinLength(2, {
        each: true
    })
    @IsString({
        each: true
    })
    public tags?: string[];

    @IsDefined()
    public category: Category;

    @IsOptional()
    public uploadedFiles?: Express.Multer.File[] | { [fieldname: string]: Express.Multer.File[] };
}