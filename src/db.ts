import { createConnection } from 'typeorm';

import { Pin } from './entities/Pin';
import { Tag } from './entities/Tag';
import { Category } from './entities/Category';
import { File } from './entities/File';

export async function getDbConnection() {

    const DATABASE_TYPE = 'mysql';

    const entities = [
        Pin,
        Tag,
        File,
        Category
    ];

    const conn = await createConnection({
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: entities,
        synchronize: true
    });

    return conn;

}