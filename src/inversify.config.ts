import { AsyncContainerModule } from "inversify";
import { Repository } from "typeorm";
import { Pin } from "./entities/Pin";
import { File } from "./entities/File";
import { Tag } from "./entities/Tag";
import { Category } from "./entities/Category";
import { getDbConnection } from "./db";
import { getPinRepository } from "./repositories/PinRepository";
import { getFileRepository } from "./repositories/FileRepository";
import { getTagRepository } from "./repositories/TagRepository";
import { getCategoryRepository } from "./repositories/CategoryRepository";
import { CategoryService } from "./services/CategoryService";
import { FileService } from "./services/FileService";
import { PinService } from "./services/PinService";
import { TagService } from "./services/TagService";
import { TYPE } from "./constants/types";


export const bindings = new AsyncContainerModule(async (bind) => {

    await getDbConnection();
    
    // Register controllers
    await require("./controllers/PinController");
    await require("./controllers/CategoryController");
    await require("./controllers/front/IndexController");


    // Register repositories
    bind<Repository<Pin>>(TYPE.PinRepository).toDynamicValue(() => {
        return getPinRepository();
    }).inRequestScope();
    bind<Repository<Tag>>(TYPE.TagRepository).toDynamicValue(() => {
        return getTagRepository();
    }).inRequestScope();
    bind<Repository<File>>(TYPE.FileRepository).toDynamicValue(() => {
        return getFileRepository();
    }).inRequestScope();
    bind<Repository<Category>>(TYPE.CategoryRepository).toDynamicValue(() => {
        return getCategoryRepository();
    }).inRequestScope();
    bind<CategoryService>(TYPE.CategoryService).to(CategoryService);
    bind<FileService>(TYPE.FileService).to(FileService);
    bind<PinService>(TYPE.PinService).to(PinService);
    bind<TagService>(TYPE.TagService).to(TagService);
});