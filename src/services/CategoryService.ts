import {
    injectable,
    inject
} from 'inversify';
import { getConnection } from "typeorm";
import { Category } from "../entities/Category";
import { TYPE } from "../constants/types";
import { Repository } from "typeorm";
import { Paginator } from '../utils/Paginator';
import { AppError } from '../utils/AppError';
import { ICategoryData } from '../models/category/ICategoryData';
import { PinService } from './PinService';

@injectable()
export class CategoryService {

    @inject(TYPE.CategoryRepository)
    private categoryRepository: Repository<Category>;

    @inject(TYPE.PinService)
    private pinService: PinService;

    public async find(id: number): Promise<Category | undefined> {
        // throw new AppError('eheh', AppError.NOT_FOUND, 404);
        return await this.categoryRepository.findOne(id);
    }

    public async create(data: ICategoryData): Promise<Category> {
        let category = await this.categoryRepository.create(data);
        return await this.categoryRepository.save(category);
    }

    public async update(id: number, data: ICategoryData): Promise<Category | undefined> {
        await this.categoryRepository.update(id, data);
        return await this.find(id);
    }

    public async delete(id: number): Promise<void> {
        let category = await this.categoryRepository.findOne(id, { relations: ["pins"]});
        if(!category) {
            return;
        }
        if (category.pins && category.pins.length > 0) {
            await Promise.all(category.pins.map(async (pin) => {
                this.pinService.delete(pin.id);
            }));
        }
        await this.categoryRepository.delete(id);
    }

    public async getPaginator(page: number = 1, records: number = 10) {
        page = +page || 1;
        records = +records || 10;
        let queryBuilder = this.categoryRepository
            .createQueryBuilder('category')
            .orderBy('id', 'DESC')
            .skip((page - 1) * records)
            .take(records);

        let total = await queryBuilder.getCount();
        let items = await queryBuilder.getMany();

        return new Paginator(page, records, total, items);
    }
}