import {
    injectable,
    inject
} from 'inversify';
import { getConnection } from "typeorm";
import { Tag } from "../entities/Tag";
import { TYPE } from "../constants/types";
import { Repository } from "typeorm";
import { Paginator } from '../utils/Paginator';
import { AppError } from '../utils/AppError';

@injectable()
export class TagService {
    @inject(TYPE.TagRepository)
    private fileRepository: Repository<Tag>
}