import {
    injectable,
    inject
} from 'inversify';
import { getConnection } from "typeorm";
import { File } from "../entities/File";
import { TYPE } from "../constants/types";
import { Repository } from "typeorm";
import { Paginator } from '../utils/Paginator';
import { AppError } from '../utils/AppError';
import { Express } from 'express';
import * as path from 'path';
import * as fs from 'fs';

@injectable()
export class FileService {
    @inject(TYPE.FileRepository)
    private fileRepository: Repository<File>

    public async create(uploadedFile: Express.Multer.File): Promise<File> {
        let file = new File();
        file.name = uploadedFile.filename;
        file.extension = path.extname(file.name);
        file.path = '/' + process.env.PUBLIC_DIR + '/' + process.env.UPLOAD_DIR + '/' + file.name;
        file.mimetype = uploadedFile.mimetype;
        file.size = uploadedFile.size;
        return await this.fileRepository.save(file);
    }

    public async delete(id: number): Promise<void> {
        let file = await this.fileRepository.findOne(id);
        if(!file) {
            return;
        }
        fs.unlink(__dirname + '/../..' + file.path);
        await this.fileRepository.delete(id);
    }
}