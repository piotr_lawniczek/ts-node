import { inject, injectable } from 'inversify';
import { Repository } from "typeorm";
import { TYPE } from "../constants/types";
import { Pin } from "../entities/Pin";
import { Tag } from '../entities/Tag';
import { FileService } from './FileService';
import { IPinData } from '../models/pin/IPinData';
import { Paginator } from '../utils/Paginator';
import { Express } from 'express';
import { PinView } from '../models/pin/PinView';
import { File } from '../entities/File';
import { Category } from '../entities/Category';

@injectable()
export class PinService {
    @inject(TYPE.PinRepository)
    private pinRepository: Repository<Pin>

    @inject(TYPE.FileService)
    private fileService: FileService

    @inject(TYPE.TagRepository)
    private tagRepository: Repository<Tag>

    @inject(TYPE.CategoryRepository)
    private categoryRepository: Repository<Category>

    public async find(id: number): Promise<Pin | undefined> {
        return this.pinRepository.findOne(id);
    }

    public async create(data: IPinData): Promise<Pin | undefined> {
        let pin = new Pin();
        pin = await this.setData(pin, data);
        pin.category = data.category;
        return await this.pinRepository.save(pin);
    }

    public async update(id: number, data: IPinData): Promise<Pin | undefined> {
        let pin = await this.pinRepository.findOne(id);
        if (!pin) {
            return;
        }
        pin = await this.setData(pin, data);
        return await this.pinRepository.save(pin);
    }

    public async delete(id: number): Promise<void> {
        let pin = await this.pinRepository.findOne(id);
        if(!pin) {
            return;
        }
        if (pin.files.length > 0) {
            await Promise.all(pin.files.map(async (file) => {
                this.fileService.delete(file.id);
            }));
        }
        pin.tags = [];
        await this.pinRepository.delete(id);
    }

    public async addFile(id: number, uploadedFile: Express.Multer.File): Promise<Pin | undefined> {
        let pin = await this.pinRepository.findOne(id);
        if (!pin) {
            return;
        }
        let file: File = await this.fileService.create(uploadedFile);
        pin.files.push(file);
        pin = await this.pinRepository.save(pin);
        return pin;
    }

    public async deleteFile(id: number, fileId: number): Promise<Pin | void> {
        let pin = await this.pinRepository.findOne(id);
        if(!pin) {
            return;
        }
        let fileIndex = pin.files.findIndex((file: File) => { return file.id == fileId });
        if(fileIndex === -1) {
            return;
        }
        let file = pin.files[fileIndex];
        pin.files.splice(fileIndex, 1);
        await this.fileService.delete(fileId);
        pin = await this.pinRepository.save(pin);
        return pin;
    }

    public async getPaginator(page: number = 1, records: number = 10, tags?: string[], categoryId?: number): Promise<Paginator> {
        page = +page || 1;
        records = +records || 10;
        let queryBuilder = this.pinRepository
            .createQueryBuilder('pin')
            .leftJoinAndSelect('pin.tags', 'tag');
        if(tags && tags.length > 0) {
            queryBuilder = queryBuilder.where("tag.name IN (:...tags)", { tags })
        }
        if(categoryId) {
            queryBuilder = queryBuilder.leftJoin('pin.category', 'category')
                .where('category.id = :categoryId', { categoryId })
        } else {
            queryBuilder = queryBuilder.leftJoinAndSelect('pin.category', 'category');
        }
        queryBuilder = queryBuilder.leftJoinAndSelect('pin.files', 'file')
            .orderBy('pin.id', 'DESC')
            .skip((page - 1) * records)
            .take(records);

        let total = await queryBuilder.getCount();
        let items = await queryBuilder.getMany();

        return new Paginator(page, records, total, items.map((pin: Pin) => { return new PinView(pin); }));
    }

    private async setData(pin: Pin, data: IPinData): Promise<Pin> {
        pin.name = data.name;
        pin.content = data.content;
        if (data.tags) {
            pin.tags = [];
            await Promise.all(data.tags.map(async (tagName) => {
                let tag = await this.tagRepository.findOne({ name: tagName });
                if (!tag) {
                    tag = await this.tagRepository.create({ name: tagName });
                    tag = await this.tagRepository.save(tag);
                }
                pin.tags.push(tag);
            }));
        }
        if (data.uploadedFiles) {
            pin.files = [];
            await Promise.all(data.uploadedFiles.map(async (uploadedFile: Express.Multer.File) => {
                let file = await this.fileService.create(uploadedFile);
                pin.files.push(file);
            }));
        }
        return pin;
    }
}