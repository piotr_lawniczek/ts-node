import { getConnection } from "typeorm";
import { Category } from "../entities/Category";

export function getCategoryRepository() {
    const conn = getConnection();
    const categoryRepository = conn.getRepository(Category);
    return categoryRepository;
}