import { getConnection } from "typeorm";
import { File } from "../entities/File";

export function getFileRepository() {
    const conn = getConnection();
    const fileRepository = conn.getRepository(File);
    return fileRepository;
}