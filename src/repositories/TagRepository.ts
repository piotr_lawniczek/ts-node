import { getConnection } from "typeorm";
import { Tag } from "../entities/Tag";

export function getTagRepository() {
    const conn = getConnection();
    const tagRepository = conn.getRepository(Tag);
    return tagRepository;
}