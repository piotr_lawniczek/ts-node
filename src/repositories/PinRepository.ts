import { getConnection } from "typeorm";
import { Pin } from "../entities/Pin";

export function getPinRepository() {
    const conn = getConnection();
    const pinRepository = conn.getRepository(Pin);
    return pinRepository;
}