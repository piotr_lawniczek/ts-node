// Require the core node modules.
import chalk = require("chalk");
import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import * as bodyParser from 'body-parser';
import { Container, inject } from 'inversify';
import { bindings } from "./inversify.config";
import * as express from "express";
import { validationErrorHandler } from './middlewares/Validator';
import { dbQueryErrorHandler } from './middlewares/DBErrorHandler';
import { appErrorHandler } from './middlewares/AppErrorHandler';
import { multipartForm } from "./middlewares/MultipartForm";

require('dotenv').config();

// Initialize the Express application.
(async () => {

    const port = process.env.PORT;
    const container = new Container();
    await container.loadAsync(bindings);
    // container.applyMiddleware(validationErrorHandler);
    const server = new InversifyExpressServer(container);
    server.setConfig((app) => {
        app.use('/', express.static(__dirname + '/../' + process.env.PUBLIC_DIR + '/'));
        app.use('/' + process.env.UPLOAD_DIR, express.static(__dirname + '/../' + process.env.PUBLIC_DIR + '/' + process.env.UPLOAD_DIR + '/'));
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());
    });

    server.setErrorConfig((app) => {
        app.use(validationErrorHandler);
        app.use(dbQueryErrorHandler);
        app.use(appErrorHandler);
    });

    const app = server.build();
    app.listen(port, () => {
        console.log(chalk.bold.green(`Server running at http://127.0.0.1:${port}/`))
    });

})();