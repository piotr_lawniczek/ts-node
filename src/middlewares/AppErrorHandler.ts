import * as express from 'express';
import { AppError } from '../utils/AppError';

interface IAppError {
    message: string;
    code: string;
    errno: number;
    stack?: string;
}

export function appErrorHandler(error: Error, request: express.Request, response: express.Response, next: express.NextFunction) {
    if (error instanceof AppError) {
        let errorResponse: IAppError = {
            message: error.message,
            code: error.code,
            errno: error.errno
        };
        if (process.env.DEBUG === 'true') {
            errorResponse.stack = error.stack;
        }
        response.status(error.httpStatus).json({
            error: errorResponse
        }).end();
    } else {
        next(error);
    }
}