import * as express from 'express';
import { QueryFailedError } from 'typeorm';

interface IQueryFailedError {
    message: string;
    code: string;
    errno: number;
}

export function dbQueryErrorHandler(error: IQueryFailedError, request: express.Request, response: express.Response, next: express.NextFunction) {
    if (error instanceof QueryFailedError) {
        if (process.env.DEBUG === 'true') {
            response.status(500).json(error);
        } else {
            response.status(500).json({
                error: {
                    message: error.message,
                    code: error.code,
                    errno: error.errno
                }
            }).end();
        }
    } else {
        next(error);
    }
}