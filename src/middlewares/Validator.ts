import * as express from 'express';
import { deserialize, plainToClass } from 'class-transformer';
import { Validator, ValidationError } from 'class-validator';

type Constructor<T> = { new(): T };

// Checking if request body passes validation rules given in class
export function validate<T>(type: Constructor<T>): express.RequestHandler {
    let validator = new Validator();

    return (request, response, next) => {
        let requestBody = plainToClass(type, request.body);
        let errors = validator.validateSync(requestBody, { whitelist: true });
        if (errors.length > 0) {
            next(errors);
        } else {
            request.body = requestBody;
            next();
        }
    }
}
// Validation error handling
export function validationErrorHandler(error: Error, request: express.Request, response: express.Response, next: express.NextFunction) {
    if (error instanceof Array && error[0] instanceof ValidationError) {
        response.status(422).json({ errors: error }).end();
    } else {
        next(error);
    }
}