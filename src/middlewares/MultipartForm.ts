import * as multer from 'multer';
import * as express from 'express';
import { deserialize, plainToClass } from 'class-transformer';
import * as crypto from 'crypto';
import * as mime from 'mime';
import * as path from 'path';
// type Constructor<T> = { new(): T };

// export function multipartForm<T>(type: Constructor<T>, fieldName: string): express.RequestHandler {
//     let upload = multer({ dest: process.env.UPLOAD_DIR }).array(fieldName);
//     return (request, response, next) => {
//         upload(request, response, next);
//         let requestBody = plainToClass(type, request.body);
//         request.body = requestBody;
//         next();
//     }
// }

function configMulterStorage() {
    let uploadDir = __dirname + '/../../' + process.env.PUBLIC_DIR + '/' + process.env.UPLOAD_DIR + '/';
    let storage = multer.diskStorage({
        destination: function (request, file, callback) {
            callback(null, uploadDir)
        },
        filename: function (request, file, callback) {
            crypto.pseudoRandomBytes(16, function (error, raw) {
                callback(null, raw.toString('hex') + Date.now() + path.extname(file.originalname));
            });
        }
    });
    return storage;
}

export function multipartForm() {
    let storage = configMulterStorage();
    return multer({ storage: storage }).any();
}

export function uploadFiles(field: string) {
    return multer({ dest: __dirname + '/../../' + process.env.PUBLIC_DIR + '/' + process.env.UPLOAD_DIR + '/' }).array(field);
}

export function uploadFile(field: string) {
    let storage = configMulterStorage();
    return multer({ storage: storage }).single(field);
}