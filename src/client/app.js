import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App.jsx';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers';
import style from './scss/main.scss';

const store = createStore(reducers, { ...window.APP_STATE });

ReactDOM.hydrate(
    <Provider store={store}> 
        <App />
    </Provider>,
    document.getElementById('app') 
  );