import * as React from 'react';
import * as PropTypes from 'prop-types';

export default class Html extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        initialState: PropTypes.object,
        scripts: PropTypes.array
    }

    render() {
        const { children, initialState, scripts } = this.props;

        return (
            <html>
                <head>
                    <meta charSet="UTF-8" />
                    <title>111111111</title>
                </head>
                <body>
                    <div id="app" dangerouslySetInnerHTML={{ __html: children }}
                    ></div>
                    {initialState && (
                        <script
                            dangerouslySetInnerHTML={{
                                __html: `window.APP_STATE=${JSON.stringify(initialState)}`
                            }}
                        ></script> 
                    )}
                    {scripts.map((item, index) => {
                        return <script key={index} src={item}></script>;
                    })}
                </body>
            </html>
        );
    }
}