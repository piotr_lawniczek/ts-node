import * as React from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CategoriesNav from './../components/CategoriesNav.jsx';
import PinsList from './../components/PinsList.jsx';
import * as _ from 'lodash';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import Action from './../actions/actions';
import { Layout, Menu, Icon, Affix, Select, Button } from 'antd';

const { Header, Content, Footer, Sider } = Layout;


class App extends React.Component {
  static propTypes = {
    shouldReloadPins: PropTypes.bool,
    shouldReloadCategories: PropTypes.bool,
    activeCategory: PropTypes.object,
    queriedTags: PropTypes.array,
    categories: PropTypes.array.isRequired,
    pins: PropTypes.array.isRequired,
    chooseCategory: PropTypes.func.isRequired,
    dismissCategory: PropTypes.func.isRequired,
    removeCategory: PropTypes.func.isRequired,
    reloadCategories: PropTypes.func.isRequired,
    searchTags: PropTypes.func.isRequired,
    getPins: PropTypes.func.isRequired,
    reloadPins: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
    this.removeCategory = this.removeCategory.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.shouldReloadPins) {
      let url = this.props.activeCategory ? `/api/categories/${this.props.activeCategory.id}/pins` : '/api/pins';
      let params = {
        page: 1,
        records: 200
      };
      if (this.props.queriedTags && this.props.queriedTags.length > 0) {
        params.tags = this.props.queriedTags;
      }
      axios.get(url, {
        params
      }).then(response => this.props.getPins(response.data.items));
    } else {
      console.log(this.props.pins);
    }

  }

  addPin() {

  }

  removeCategory() {
    axios.delete(`/api/categories/${this.props.activeCategory.id}`).then(response => this.props.removeCategory(this.props.activeCategory));
  }

  render() {
    return (
      <Layout>
        <Sider
          style={{ overflow: 'auto', height: '100vh', width: 250 }}
          breakpoint="lg"
          collapsedWidth="0"
        >
          <CategoriesNav categories={this.props.categories} reload={this.props.reloadCategories} choose={this.props.chooseCategory} />
        </Sider>
        <div className="content">
          <Layout>
            <Affix offsetTop={0}>
              <Header style={{ background: '#fff', padding: 0, textAlign: 'center' }}>
                <div className="header-active-category">
                  {this.props.activeCategory && <span>{this.props.activeCategory.name}<Icon onClick={this.props.dismissCategory} type="close" style={{ marginLeft: '15px' }} /></span>}
                  {!this.props.activeCategory && 'All categories'}
                  <div className="header-controls">
                    <Select
                      mode="tags"
                      style={{ width: '100%' }}
                      placeholder="Search by tags"
                      onChange={this.props.searchTags}
                    >
                    </Select>
                    {this.props.activeCategory &&
                      <div className="header-controls-btns">
                        <Button type="primary" className="menu-button" onClick={this.addPin.bind(this)} style={{ display: 'inline', margin: '0px' }}>
                          <Icon type="plus" />Add pin
                        </Button>
                        <Button type="danger" className="menu-button" onClick={this.removeCategory} style={{ display: 'inline', margin: '0px' }}>
                          <Icon type="close" />Remove category
                      </Button>
                      </div>}
                  </div>
                </div>
              </Header>
            </Affix>
            <Content style={{ margin: '24px 16px 0' }}>
              <PinsList activeCategory={this.props.activeCategory} pins={this.props.pins} reload={this.props.reloadPins} />
            </Content>
          </Layout>
        </div>
      </Layout>
    );
  }
}

export default connect(state => ({ ...state }), dispatch => bindActionCreators(Action, dispatch))(App);