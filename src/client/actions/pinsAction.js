export function reloadPins() {
    return {
        type: 'RELOAD_PINS'
    }
}

export function getPins(pins) {
    return {
        type: 'GET_PINS',
        pins
    }
}

export function searchTags(tags) {
    return {
        type: 'SEARCH_TAGS',
        tags
    }
}