export function reloadCategories() {
    return {
        type: 'RELOAD_CATEGORIES'
    }
}

export function chooseCategory(category) {
    return {
        type: 'CHOOSE_CATEGORY',
        category
    }
}

export function dismissCategory() {
    return {
        type: 'DISMISS_CATEGORY'
    }
}

export function removeCategory() {
    return {
        type: 'REMOVE_CATEGORY'
    }
}