import * as pinsAction from './pinsAction'
import * as categoriesAction from './categoriesAction'

export default {
  ...pinsAction,
  ...categoriesAction,
}