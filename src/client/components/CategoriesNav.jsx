import * as React from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout, Menu, Icon, Tooltip, Button, Affix } from 'antd';
const { Header, Content, Footer, Sider } = Layout;

export default class CategoriesNav extends React.Component {
    static propTypes = {
        categories: PropTypes.array.isRequired,
        reload: PropTypes.func.isRequired,
        choose: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        this.state = { categories: this.props.categories };
        console.log(this.state.categories);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ categories: nextProps.categories})
    }

    addCategory(event) {

        // console.log(event);
        console.log(this);
    }

    chooseCategory(category) {
        this.props.choose(category);
    }

    render() {
        return (

            <div className='categories-list'>
                <Affix offsetTop={0}>
                    <Button type="primary" className="menu-button" onClick={this.addCategory.bind(this)} style={{ display: 'block', margin: '20px auto' }}>
                        <Icon type="plus" />Add category
                    </Button>
                </Affix>
                <Menu
                    style={{ backgroundColor: '#480A4C' }}
                    mode="inline"
                    theme="dark"
                >
                    {
                        this.state.categories.map((category, index) => {
                            return <Menu.Item key={index} id={category.id} onClick={() => { return this.chooseCategory(category) }}>
                                <Tooltip title={category.description}>
                                    <span className="nav-text">{category.name}</span>
                                </Tooltip>
                            </Menu.Item>
                        })
                    }
                </Menu>
            </div>
        );
    }
}