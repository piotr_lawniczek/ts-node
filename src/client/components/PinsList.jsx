import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Icon, Card, Tag, Input, Divider } from 'antd';
import * as _ from 'lodash';
import Pin from './Pin.jsx';
import axios from 'axios';



export default class PinsList extends React.Component {
    static propTypes = {
        pins: PropTypes.array.isRequired,
        reload: PropTypes.func.isRequired,
        activeCategory: PropTypes.object
    }

    constructor(props) {
        super(props);
        this.state = {
            pins: this.props.pins,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ pins: nextProps.pins })
    }

    updatePin(pin) {
        let pins = this.state.pins;
        let toChange = _.find(pins, item => item.id === pin.id);
        toChange = pin;
        let requestPin = { ...pin };
        if (!pin.category) {
            requestPin.category = this.props.activeCategory;
        }
        axios.put(`/api/pins/${pin.id}`, requestPin).then(
            response => this.props.reload()
        );
        // this.setState({
        //     pins: pins
        // })

    }

    deletePin(pin) {
        let pins = this.state.pins;
        _.remove(pins, item => item.id === pin.id);
        axios.delete(`/api/pins/${pin.id}`).then(
            response => this.props.reload()
        );
        // this.setState({
        //     pins
        // })
        // this.props.reload();
    }

    render() {
        return (
            <div className='pins-list'>
                {
                    this.state.pins.map((pin, index) => {
                        return <Pin pin={pin} update={() => this.updatePin(pin)} delete={() => this.deletePin(pin)} />
                    })
                }
            </div>
        );
    }
}