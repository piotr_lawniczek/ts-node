import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Button, Icon, Card, Tag, Input, Divider } from 'antd';
import * as _ from 'lodash';

export default class Pin extends React.Component {
    static propTypes = {
        pin: PropTypes.object.isRequired,
        update: PropTypes.func.isRequired,
        delete: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        console.log('pin cons');
        this.state = {
            pin: this.props.pin,
            tagInputVisible: false,
            tagInputValue: '',
        };
        this.savePin = this.savePin.bind(this);
        this.deletePin = this.deletePin.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ pin: nextProps.pin})
    }

    saveTagInputRef = input => this.input = input

    showTagInput = () => {
        this.setState({ tagInputVisible: true }, () => this.input.focus());
    }

    handleTagInputChange = (e) => {
        this.setState({ tagInputValue: e.target.value });
    }

    handlePinContentChange = (e) => {
        let pin = this.state.pin;
        pin.content = e.target.value;
        this.setState({ pin: pin });
    }

    handleTagInputConfirm = () => {
        const tagInputValue = this.state.tagInputValue;
        let pin = this.state.pin;
        if (tagInputValue && pin.tags.indexOf(tagInputValue) === -1) {
            pin.tags = [...pin.tags, tagInputValue];
        }
        this.setState({
            pin,
            tagInputVisible: false,
            tagInputValue: '',
        });
    }

    savePin() {
        this.props.update(this.state.pin);
    }

    deletePin() {
        this.props.delete(this.state.pin);
    }

    deleteTag(tag) {
        let pin = this.state.pin;
        _.remove(pin.tags, item => item === tag);
        this.setState({
            pin: pin
        })
    }

    render() {
        return (
            // <div className="pin">
            <Card title={this.state.pin.name} style={{ margin: '30px 0' }}
                actions={[
                    <div className="pin-actions">
                        <Button type="danger" onClick={this.deletePin}>
                            <Icon type="close" />Delete
                    </Button>,
                    <Button type="primary" onClick={this.savePin}>
                            <Icon type="check" />Save
                    </Button>
                    </div>
                ]}>
                <Input.TextArea style={{ margin: '0 0 15px' }} autosize value={this.state.pin.content} onChange={this.handlePinContentChange} />
                {
                    <div className="pin-tags">
                        {
                            this.state.pin.tags.map((tag, index) => {
                                return <Tag closable onClose={() => this.deleteTag(tag)}>{tag}</Tag>
                            })
                        }
                        {this.state.tagInputVisible && (
                            <Input
                                ref={this.saveTagInputRef}
                                type="text"
                                size="small"
                                style={{ width: 78 }}
                                value={this.state.tagInputValue}
                                onChange={this.handleTagInputChange}
                                onBlur={this.handleTagInputConfirm}
                                onPressEnter={this.handleTagInputConfirm}
                            />
                        )}
                        {!this.state.tagInputVisible && (
                            <Tag
                                onClick={this.showTagInput}
                                style={{ background: '#fff', borderStyle: 'dashed' }}
                            >
                                <Icon type="plus" /> New Tag
                            </Tag>
                        )}
                    </div>
                }
            </Card>
        );
    }
}