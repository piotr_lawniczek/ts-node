import * as _ from 'lodash';

export default function reducer(state, action) {
  switch (action.type) {
    case 'CHOOSE_CATEGORY':
      return { ...state,
        activeCategory: action.category,
        shouldReloadPins: true,
        shouldReloadCategories: false,
      };
    case 'DISMISS_CATEGORY':
      return { ...state,
        activeCategory: null,
        shouldReloadPins: true,
        shouldReloadCategories: false,
      }
    case 'REMOVE_CATEGORY':
      let newState = { ...state
      };
      _.remove(newState.categories, category => category == state.activeCategory);
      return { ...newState,
      // return { ...state,
        activeCategory: null,
        shouldReloadCategories: true,
        shouldReloadPins: true
      }
    case 'SEARCH_TAGS':
      return { ...state,
        queriedTags: action.tags,
        shouldReloadPins: true,
        shouldReloadCategories: false,
      }
    case 'GET_PINS':
      return { ...state,
        pins: action.pins,
        shouldReloadPins: false
      }
    case 'RELOAD_CATEGORIES':
      return { ...state,
        shouldReloadCategories: false,
        shouldReloadPins: false
      };
    case 'RELOAD_PINS':
      return { ...state,
        shouldReloadCategories: false,
        shouldReloadPins: false
      };
    default:
      return { ...state
      };
  }
}